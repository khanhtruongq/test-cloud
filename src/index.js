// import * as wdio from 'webdriverio';
// import * as assert from 'assert';
const wdio = require('webdriverio');
const assert = require('assert');
const find = require('appium-flutter-finder');

// desiredCaps = {
//   // Set your BrowserStack access credentials
//   'browserstack.user' : 'trngquckhnh_6LPsKn',
//   'browserstack.key' : 'C9qMpYDJ6pC9sr8ScThc',

//   // Set URL of the application under test
//   'app' : 'bs://abd5cd8f41d4c45591364a921c6e0fc945f89dd4',

//   // Specify device and os_version for testing
//   'device' : 'Google Pixel 3',
//   'os_version' : '9.0',

//   // Set other BrowserStack capabilities
//   'project' : 'First NodeJS project',
//   'build' : 'Node Android',
//   'name': 'first_test'
// };


// var osSpecificOps = {
//   "os_version": "11.0",
//   "device": "Google Pixel 4",
//   "real_mobile": "true",
//   "browserstack.local": "false",
//   "browserstack.debug": "true",
//   "browserstack.console": "verbose",
//   "browserstack.user": "trngquckhnh_6LPsKn",
//   "browserstack.key": "C9qMpYDJ6pC9sr8ScThc",
//   "browserName": "Android",
//   app: 'bs://abd5cd8f41d4c45591364a921c6e0fc945f89dd4',
// }

// var osSpecificOps = {
//   "os_version": "14",
//   "device": "iPhone 12",
//   "real_mobile": "true",
//   "browserstack.local": "false",
//   "browserstack.debug": "true",
//   "browserstack.user": "trngquckhnh_6LPsKn",
//   "browserstack.key": "C9qMpYDJ6pC9sr8ScThc",
//   "browserName": "iPhone"
// }



// const osSpecificOps =
//   process.env.APPIUM_OS === 'android'
//     ? {
//         platformName: 'Android',
//         deviceName: 'emulator-5554',
//         // @todo support non-unix style path
//         app: __dirname + '/../apps/android-real-debug.apk'
//       }
//     : process.env.APPIUM_OS === 'ios'
//     ? {
//         platformName: 'iOS',
//         platformVersion: '12.2',
//         deviceName: 'iPhone X',
//         noReset: true,
//         app: __dirname + '/../apps/Runner.zip'
//       }
// : {};

// const opts = {
//   user: 'trngquckhnh_6LPsKn',
//   key: 'C9qMpYDJ6pC9sr8ScThc',
//   host: 'hub.browserstack.com',
//   logLevel: 'warn',

//   connectionRetryTimeout: 90000,
//   connectionRetryCount: 3,
//   capabilities: {
//     ...osSpecificOps,
//     automationName: 'Flutter'
//   }
// };

var osSpecificOps = {
  "platformName" : "Android",
  "appium:app": "https://github.com/truongsinh/appium-flutter-driver/releases/download/v0.0.4/android-real-debug.apk",
  "appium:platformVersion" : 8.0,
  "appium:deviceName": "Android Emulator",
  "appium:automationName" : "Appium"
}


const opts = {
  user: 'oauth-khanh.truongq-73576',
  key: '4d5403fe-f0a4-4d9f-9042-c3f0d3f4f925',
  host: 'ondemand.us-west-1.saucelabs.com',
  logLevel: 'warn',

  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  capabilities: {
    ...osSpecificOps,
    automationName: 'Flutter'
  }
};




(async () => {
  const counterTextFinder = find.byValueKey('counter');
  const buttonFinder = find.byValueKey('increment');

  const driver = wdio.remote(opts);

  /* new example
  if (process.env.APPIUM_OS === 'android') {
    await driver.switchContext('NATIVE_APP');
    await (await driver.$('~fab')).click();
    await driver.switchContext('FLUTTER');
  } else {
    console.log(
      'Switching context to `NATIVE_APP` is currently only applicable to Android demo app.'
    );
  }
  */

  // assert.strictEqual(await driver.getElementText(counterTextFinder), '0');

  await driver.elementClick(buttonFinder);
  await driver.touchAction({
    action: 'tap',
    element: { elementId: buttonFinder }
  });
  await driver.elementClick(buttonFinder);
  await driver.touchAction({
    action: 'tap',
    element: { elementId: buttonFinder }
  });
  await driver.elementClick(buttonFinder);
  await driver.touchAction({
    action: 'tap',
    element: { elementId: buttonFinder }
  });
  await driver.elementClick(buttonFinder);
  await driver.touchAction({
    action: 'tap',
    element: { elementId: buttonFinder }
  });
  await driver.elementClick(buttonFinder);
  await driver.touchAction({
    action: 'tap',
    element: { elementId: buttonFinder }
  });

  assert.strictEqual(await driver.getElementText(counterTextFinder), '10');

  await driver.elementClick(find.byTooltip('Increment'));

  assert.strictEqual(
    await driver.getElementText(
      find.descendant({
        of: find.byTooltip('counter_tooltip'),
        matching: find.byValueKey('counter')
      })
    ),
    '3'
  );

  await driver.elementClick(find.byType('FlatButton'));
  // await driver.waitForAbsent(byTooltip('counter_tooltip'));

  assert.strictEqual(
    await driver.getElementText(find.byText('This is 2nd route')),
    'This is 2nd route'
  );

  await driver.elementClick(find.pageBack());

  assert.strictEqual(
    await driver.getElementText(
      find.descendant({
        of: find.ancestor({
          of: find.bySemanticsLabel(RegExp('counter_semantic')),
          matching: find.byType('Tooltip')
        }),
        matching: find.byType('Text')
      })
    ),
    '3'
  );

  driver.deleteSession();
})();
