exports.config = {
    user: 'trngquckhnh_6LPsKn',
    key: 'C9qMpYDJ6pC9sr8ScThc',
  
    updateJob: false,
    specs: [
      './src/index.js'
    ],
    exclude: [],
  
    capabilities: [{
      project: "First Webdriverio Android Project",
      build: 'Webdriverio Android',
      name: 'first_test',
      device: 'Google Pixel 3',
      os_version: "9.0",
      app: 'bs://abd5cd8f41d4c45591364a921c6e0fc945f89dd4',
      'browserstack.debug': true
    }],
  
    logLevel: 'info',
    coloredLogs: true,
    screenshotPath: './errorShots/',
    baseUrl: '',
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
  
    framework: 'mocha',
    mochaOpts: {
      ui: 'bdd',
      timeout: 20000
    }
  };